package testRunner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import handler.FileReaderHandler;
import java.io.File;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
//import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.vimalselvam.cucumber.listener.Reporter;


@CucumberOptions(
        features = "src/features",
        glue={"stepDefinition"},
        tags = {"@P2"},
        plugin = { 
//        		"usage",
//        		"pretty" ,"html:target/cucumber-reports",
        		"com.vimalselvam.cucumber.listener.ExtentCucumberFormatter:target/ExtentReport/Report.html"
        		},
       monochrome = true
)

public class CucumberRunnerTestScenario{
	
	   private TestNGCucumberRunner testNGCucumberRunner;
	   
	   
//	    @Parameters("reportname")
	    @BeforeClass(alwaysRun = true)
	    public void setUpClass() throws Exception {
	        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
//	        ExtentProperties extentProperties = ExtentProperties.INSTANCE;
//	        extentProperties.setReportPath("output/"+reportname);	        
	    }

	    @Test(groups = "cucumber", description = "Runs Cucumber Feature", dataProvider = "feature")
	    public void feature(CucumberFeatureWrapper cucumberFeature) {
	        testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
	    }

	    @DataProvider
	    public Object[][] feature() {
	        return testNGCucumberRunner.provideFeatures();
	    }

	    
		@AfterClass(alwaysRun = true)
	    public void tearDownClass() throws Exception {
			testNGCucumberRunner.finish();
//	    	Reporter.loadXMLConfig(new File("C:/Users/skbeh/cucumber-workspace/BDD-Parallel-TestNG/config/extent-config.xml"));
//	    	Reporter.getExtentHtmlReport();
	       
	    }
	    
}