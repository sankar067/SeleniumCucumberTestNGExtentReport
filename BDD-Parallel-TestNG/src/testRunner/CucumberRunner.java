package testRunner;

import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import handler.FileReaderHandler;

import java.io.File;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
//import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


@CucumberOptions(
        features = "src/features",
        glue={"stepDefinition"},
        tags = {"@P1"},
        plugin = { 
//        		"usage",
//        		"pretty" ,"html:target/cucumber-reports",
        		"com.vimalselvam.cucumber.listener.ExtentCucumberFormatter:target/ExtentReport/Report.html"
        		},
       monochrome = true
)

public class CucumberRunner extends AbstractTestNGCucumberTests {
	
	   private TestNGCucumberRunner testNGCucumberRunner;
	   
//	    @Parameters("reportname")
	    @BeforeClass(alwaysRun = true)
	    public void setUpClass() throws Exception {
	        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
//	        ExtentProperties extentProperties = ExtentProperties.INSTANCE;
//	        extentProperties.setReportPath("output/"+reportname);
	    }

	    @Test(groups = "cucumber", description = "Runs Cucumber Feature", dataProvider = "features")
	    public void feature(CucumberFeatureWrapper cucumberFeature) {
	        testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
	        
	    }

	    @DataProvider
	    public Object[][] features() {
	        return testNGCucumberRunner.provideFeatures();
	    }

	    
		@AfterClass
	    public void tearDownClass() throws Exception {
			testNGCucumberRunner.finish(); 
//			System.out.println("File path"+FileReaderHandler.getInstance().getConfigReader().getReportConfigPath());
//	    	Reporter.loadXMLConfig(new File(FileReaderHandler.getInstance().getConfigReader().getReportConfigPath()));
//	    	Reporter.setSystemInfo("User Name", System.getProperty("user.name"));
//	    	Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
//	    	Reporter.setSystemInfo("Machine", 	"Windows 10" + "64 Bit");
//	    	Reporter.setSystemInfo("Selenium", "3.7.0");
//	    	Reporter.setSystemInfo("Java Version", "1.8.0_151");
//	    	Reporter.setTestRunnerOutput("Sample test runner output message");
//	    	Reporter.getExtentHtmlReport();
	        
	    }
	    

}