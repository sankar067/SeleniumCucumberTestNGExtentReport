package handler;

import dataProvider.ConfigFileReader;

public class FileReaderHandler {

    private static FileReaderHandler FileReaderHandler = new FileReaderHandler();
    private static ConfigFileReader configFileReader;

    private FileReaderHandler() {
    }

    public static FileReaderHandler getInstance( ) {
        return FileReaderHandler;
    }

    public ConfigFileReader getConfigReader() {
        return (configFileReader == null) ? new ConfigFileReader() : configFileReader;
    }
}