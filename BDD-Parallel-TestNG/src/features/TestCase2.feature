@P2
Feature: Test Scenario Mercury Site Automation Feature-2

  Background:
    Given Launch Browser "Chrome" in Local environment
    Given Navigate to Home Page "http://newtours.demoaut.com/"

  Scenario: Mercury Demo Site - End-To-End Flow
    #Given Navigate to Home Page "http://newtours.demoaut.com/"
    When Validate Login screen
    And Login to Mercury Site using username as "mercury" and password as "mercury"
    Then Login Successful
    And Close Browser