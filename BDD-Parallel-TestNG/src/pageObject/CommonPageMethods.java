package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class CommonPageMethods {

    WebDriver driver;

    public CommonPageMethods(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void select_value_from_dropdown(WebElement sPropertyObject, String sValue){
        Select list_pc = new Select(sPropertyObject);
        list_pc.selectByValue(sValue);
    }
}
